require "dependabot/file_fetchers"
require "dependabot/file_parsers"
require "dependabot/update_checkers"
require "dependabot/file_updaters"
require "dependabot/pull_request_creator"

class Project < ApplicationRecord

  def dependencies
    host = 'gitlab.com'

    credentials = [
      {
        'type' => 'git_source',
        'host' => host,
        'username' => 'x-access-token',
        'password' => ENV['GITLAB_ACCESS_TOKEN']
      }
    ]

    source = Dependabot::Source.new(
      provider: 'gitlab',
      hostname: host,
      api_endpoint: "https://#{host}/api/v4",
      repo: repo,
      directory: '/'
    )

    fetcher = Dependabot::FileFetchers.for_package_manager(package_manager).new(
      source: source,
      credentials: credentials
    )

    parser = Dependabot::FileParsers.for_package_manager(package_manager).new(
      dependency_files: fetcher.files,
      source: source,
      credentials: credentials
    )

    parser.parse.select(&:top_level?).map do |dependency|
      checker = Dependabot::UpdateCheckers.for_package_manager(package_manager).new(
        dependency: dependency,
        dependency_files: fetcher.files,
        credentials: credentials
      )

      current_version = Gem::Version.new(dependency.version)
      latest_version = Gem::Version.new(checker.latest_version)

      {
        name: dependency.name,
        current_version: current_version.to_s,
        latest_version: latest_version.to_s,
        status: status(current_version, latest_version)
      }
    end
  end

  private

  def status(current_version, latest_version)
    if (current_version.canonical_segments[0] || 0) < (latest_version.canonical_segments[0] || 0)
      :error
    elsif (current_version.canonical_segments[1] || 0) < (latest_version.canonical_segments[1] || 0)
      :warning
    else
      :ok
    end
  end
end
