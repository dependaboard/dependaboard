class AddPackageManagerToProjects < ActiveRecord::Migration[6.1]
  def change
    add_column :projects, :package_manager, :string
  end
end
