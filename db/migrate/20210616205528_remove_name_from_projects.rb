class RemoveNameFromProjects < ActiveRecord::Migration[6.1]
  def change
    remove_column :projects, :name
  end
end
